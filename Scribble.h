#ifndef SCRIBBLE_H
#define SCRIBBLE_H

#include "Point.h"

class Scribble {
    Point points[5000];
    int pCounter;

    float leftX;
    float rightX;
    float topY;
    float bottomY;

    bool selected;

public:
    Scribble() {
        pCounter = 0;
        leftX = 1.0f;
        rightX = -1.0f;
        topY = -1.0f;
        bottomY = 1.0f;
        selected = false;
    }

    void select() {
        selected = true;
    }

    void deselect() {
        selected = false;
    }

    void addPoint(float x, float y, Color color) {
        points[pCounter] = Point(x, y, color);
        pCounter++;

        if (x < leftX) {
            leftX = x;
        }
        if (x > rightX) {
            rightX = x;
        }
        if (y < bottomY) {
            bottomY = y;
        }
        if (y > topY) {
            topY = y;
        }
    }

    void draw() {
        for (int i = 0; i < pCounter; i++) {
            points[i].draw();
        }

        if (selected) {
            glColor3f(0.0f, 0.0f, 0.0f);
            glLineWidth(2.0f);

            glBegin(GL_LINES);
                glVertex2f(leftX - 0.02f, topY + 0.02f);
                glVertex2f(rightX + 0.02f, topY + 0.02f);

                glVertex2f(rightX + 0.02f, topY + 0.02f);
                glVertex2f(rightX + 0.02f, bottomY - 0.02f);

                glVertex2f(rightX + 0.02f, bottomY - 0.02f);
                glVertex2f(leftX - 0.02f, bottomY - 0.02f);

                glVertex2f(leftX - 0.02f, bottomY - 0.02f);
                glVertex2f(leftX - 0.02f, topY + 0.02f);
            glEnd();
        }
    }

    bool contains(float x, float y) {
        if (x <= rightX && x >= leftX && y <= topY && y >= bottomY) {
            return true;
        } else {
            return false;
        }
    }
};

#endif