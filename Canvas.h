#ifndef CANVAS_H
#define CANVAS_H

#include "Rectangle.h"
#include "Toolbar.h"
// #include "Point.h"

#include "Scribble.h"

struct Canvas {
    Rectangle area;

    // Point points[1000];
    // int pCounter;

    Scribble scribbles[1000];
    int scribbleCounter;
    int selectedScribble;

    Rectangle squares[1000];
    int sCounter;

    int selectedSquare;
    float offsetX;
    float offsetY;

    Canvas(){
        area = Rectangle(-0.8f, 1.0f, 1.8f, 1.8f, Color(1.0f, 1.0f, 1.0f));
        // pCounter = 0;
        scribbleCounter = 0;
        selectedScribble = -1;
        sCounter = 0;
        selectedSquare = -1;
    }

    void handleMouseClick(float x, float y, Tool tool, Color color){
        if (tool == PENCIL){
            // points[pCounter] = Point(x, y, color);
            // pCounter++;

            scribbles[scribbleCounter] = Scribble();
            scribbleCounter++;
            scribbles[scribbleCounter - 1].addPoint(x, y, color);
        }
        else if (tool == ERASER){
            // points[pCounter] = Point(x, y, Color(1.0f, 1.0f, 1.0f), 20.0f);
            // pCounter++;
        }
        else if (tool == SQUARE){
            squares[sCounter] = Rectangle(x, y, 0.2f, 0.2f, color);
            sCounter++;
        } else if (tool == MOUSE) {
            for (int i = 0; i < sCounter; i++) {
                squares[i].deselect();
            }

            selectedSquare = -1;
            for (int i = sCounter - 1; i >= 0; i--) {
                if (squares[i].contains(x, y)) {
                    squares[i].select();
                    selectedSquare = i;
                    offsetX = x - squares[i].getX();
                    offsetY = squares[i].getY() - y;
                }
            }

            for (int i = 0; i < scribbleCounter; i++) {
                scribbles[i].deselect();
            }

            selectedScribble = -1;
            for (int i = scribbleCounter - 1; i >= 0; i--) {
                if (scribbles[i].contains(x, y)) {
                    scribbles[i].select();
                    selectedScribble = i;
                }
            }
        }
    }

    void handleMouseMotion(float x, float y, Tool tool, Color color) {
        if (tool == PENCIL){
            // points[pCounter] = Point(x, y, color);
            // pCounter++;

            scribbles[scribbleCounter - 1].addPoint(x, y, color);
        }
        else if (tool == ERASER){
            // points[pCounter] = Point(x, y, Color(1.0f, 1.0f, 1.0f), 20.0f);
            // pCounter++;
        }
        if (tool == MOUSE) {
            if (selectedSquare != -1) {
                squares[selectedSquare].setX(x - offsetX);
                squares[selectedSquare].setY(y + offsetY);
            }
        }
    }

    void draw(){
        area.draw();

        // for (int i = 0; i < pCounter; i++){
        //     points[i].draw();
        // }

        for (int i = 0; i < scribbleCounter; i++) {
            scribbles[i].draw();
        }

        for (int i = 0; i < sCounter; i++){
            squares[i].draw();
        }
    }

    bool contains(float x, float y){
        return area.contains(x, y);
    }
};

#endif